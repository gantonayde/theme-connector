#!/usr/bin/env python3
# Theme Connector for Snaps

import subprocess
import gi
gi.require_version('Gtk', '3.0')

from gi.repository import Gtk


# Get themes
get_themes = subprocess.run("snap list | grep -E '(theme|adapta-gtk-snap)' | awk '{print $1}'",
                             shell=True,
                             stdout=subprocess.PIPE,
                             encoding='utf-8')
themes_list = get_themes.stdout.strip().split("\n") 


# Get plugged apps
get_plugged_apps = subprocess.run("snap connections gtk-common-themes | grep gtk-3-themes | awk '{print $2}'",
                             shell=True,
                             stdout=subprocess.PIPE,
                             encoding='utf-8')
plugged_apps_list = get_plugged_apps.stdout.strip().split("\n")
plugged_apps_list = [i.replace(':gtk-3-themes','') for i in plugged_apps_list]

# Connect or disconnect theme
def install_theme(process, theme_comp, snap_app, theme_name, temp_pass):
    # process = 'connect' or 'disconnect'
    # theme_type = theme component 'gtk-3' or 'icons'
    # snap_app = element from plugged_apps_list
    # theme_name = name of the theme snap package
    # temp_pass = sudo password
    connect_cmd = "echo " + temp_pass + ' | ' + "sudo -S snap " + process + " " + snap_app + ':' + theme_comp + '-themes' + " " + theme_name + ':' + theme_comp + '-themes'
    subprocess.run(connect_cmd, shell=True)


# Create the main window
class MainWindow(Gtk.Window):
    def __init__(self, themes):
        Gtk.Window.__init__(self, title="Theme Connector For Snaps")
        self.resize(500,100)
        self.set_border_width(10)
        
        # Boxes
        self.main_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        self.add(self.main_vbox)
        self.entry_combo_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        self.buttons_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.main_vbox.pack_start(self.entry_combo_vbox, False, False, 0)
        self.main_vbox.pack_start(self.buttons_hbox, False, False, 0)

        # Label
        self.label = Gtk.Label()
        self.label.set_text("Please select a GTK theme package:")
        self.entry_combo_vbox.pack_start(self.label, True, True, 0)

        # Combobox with available themes
        self.themes = themes
        self.themes_combo = Gtk.ComboBoxText()
        self.themes_combo.set_entry_text_column(0)
        self.themes_combo.connect("changed", self.theme_selector)
        for theme in self.themes:
            self.themes_combo.append_text(theme)
        self.entry_combo_vbox.pack_start(self.themes_combo, False, False, 0)

        # Action buttons
        ## Connect
        self.button = Gtk.Button.new_with_label("Connect")
        self.button.connect("clicked", self.on_connect_clicked)
        self.buttons_hbox.pack_start(self.button, True, True, 0)
        ## Disconnect
        self.button = Gtk.Button.new_with_mnemonic("Disconnect")
        self.button.connect("clicked", self.on_disconnect_clicked)
        self.buttons_hbox.pack_start(self.button, True, True, 0)
        ## Close
        self.button = Gtk.Button.new_with_mnemonic("Close")
        self.button.connect("clicked", self.on_close_clicked)
        self.buttons_hbox.pack_start(self.button, True, True, 0)

    def theme_selector(self, combo):
        theme = combo.get_active_text()
        #if theme is not None:
            #print("Selected theme: %s" % theme)
        return theme

    def on_connect_clicked(self, button):
        temp_pass = get_sudo_pass()
        theme = self.theme_selector(self.themes_combo)
        for i in plugged_apps_list:
            install_theme('connect', 'gtk-3', i, theme, temp_pass)
            if i == 'snap-store':
                install_theme('connect', 'icon', i, theme, temp_pass)                
            
    def on_disconnect_clicked(self, button):
        temp_pass = get_sudo_pass()
        theme = self.theme_selector(self.themes_combo)
        for i in plugged_apps_list:
            install_theme('disconnect', 'gtk-3', i, theme, temp_pass)
            if i == 'snap-store':
                install_theme('disconnect', 'icon', i, theme, temp_pass)

    def on_close_clicked(self, button):
        print("Closing application.")
        Gtk.main_quit()

                          
# Make a window for temporary sudo password
class SudoPassWindow(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self, title="Authentication required")
        self.resize(500,100)
        self.set_border_width(10)
        self.timeout_id = None

        # Boxes
        self.main_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        self.add(self.main_vbox)
        self.entry_vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        self.buttons_hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=6)
        self.main_vbox.pack_start(self.entry_vbox, False, False, 0)
        self.main_vbox.pack_start(self.buttons_hbox, False, False, 0)

        # Label and Entry
        self.label = Gtk.Label()
        self.label.set_text("Please enter password:")
        self.entry_vbox.pack_start(self.label, True, True, 0)

        self.entry = Gtk.Entry()
        self.entry.set_text("")
        self.entry.set_visibility(False)
        self.entry_vbox.pack_start(self.entry, True, True, 0)

        # Action buttons
        ## Close
        self.button = Gtk.Button.new_with_mnemonic("Close")
        self.button.connect("clicked", self.on_close_clicked)
        self.buttons_hbox.pack_start(self.button, True, True, 0)
        ## Authenticate
        self.button = Gtk.Button.new_with_mnemonic("Authenticate")
        self.button.connect("clicked", self.on_authenticate_toggled)
        self.buttons_hbox.pack_start(self.button, True, True, 0)

    def on_close_clicked(self, button):
        self.destroy()
        Gtk.main_quit()

    def on_authenticate_toggled(self, button):
        self.temp_pass = self.entry.get_text()
        self.destroy()
        Gtk.main_quit()
        

def get_sudo_pass():
    win = SudoPassWindow()
    win.connect("delete-event", Gtk.main_quit)
    win.show_all()
    Gtk.main()
    #win.destroy()
    return win.temp_pass


def main_window():
    window = MainWindow(themes_list)
    window.connect("delete-event", Gtk.main_quit)
    window.show_all()
    Gtk.main()


if __name__ == "__main__":
    main_window()
